#!/bin/bash
##############################################################################
# Copyright 2020 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
##############################################################################

# Check system configuration.
sysroot=$(rustc --print sysroot)
libdir=${sysroot}/lib

# Exit if dependencies exist.
[[ ! -z `find ${libdir}/ -name "libz.*"` ]] && exit 1
[[ ! -z `find ${libdir}/ -name "libevent*"` ]] && exit 1
[[ ! -z `find ${libdir}/ -name "libssl*"` ]] && exit 1
[[ ! -z `find ${libdir}/ -name "libcrypto*"` ]] && exit 1

export LIBRARY_PATH=$libdir:${OUT}/lib

# Build zlib with proper instrumentation.
cd ${SRC}/zlib
CFLAGS="-g3 -O1" ./configure --debug --prefix=${OUT}
make -j$(nproc) all
make install

# Build libevent with proper instrumentation.
cd ${SRC}/libevent
sh autogen.sh
CFLAGS="-g3 -O1 -DUSE_DEBUG" ./configure --prefix=${OUT} --disable-silent-rules
make -j$(nproc) all
make install

# Build OpenSSL with proper instrumentation.
cd ${SRC}/openssl
OPENSSL_CONFIGURE_FLAGS=""
if [[ $CFLAGS = *sanitize=memory* ]]
then
  OPENSSL_CONFIGURE_FLAGS="no-asm"
fi

CFLAGS="-g3 -O1" ./config --debug --prefix=${OUT} \
    --with-zlib-include=${OUT}/include zlib-dynamic shared \
    enable-tls1_3 enable-rc5 enable-md2 enable-ec_nistp_64_gcc_128 enable-ssl3 \
    enable-ssl3-method enable-nextprotoneg enable-weak-ssl-ciphers \
    -fno-sanitize=alignment $OPENSSL_CONFIGURE_FLAGS

make -j$(nproc) all
make install_sw # skipping install_docs
make install_ssldirs

# We need to run configure with leak-checking disabled, or many of the
# test functions will fail.
export ASAN_OPTIONS=detect_leaks=0

# Build tor and the fuzz targets.
export USE_RUST=1
export TOR_RUST_DEPENDENCIES=${SRC}/tor-rust-dependencies/crates
cd ${SRC}/tor
sh autogen.sh
./configure --prefix=${OUT} --disable-asciidoc --enable-rust \
    --enable-oss-fuzz --disable-memory-sentinels \
    --with-libevent-dir=${OUT}/lib \
    --with-openssl-dir=${OUT}/lib \
    --with-zlib-dir=${OUT}/lib \
    --disable-gcc-hardening --disable-silent-rules

make build-rust
make -j$(nproc) oss-fuzz-fuzzers

TORLIBS="`make show-testing-libs`"
TORLIBS="$TORLIBS -lm -Wl,-Bstatic -ltor_rust -Lsrc/rust/target/release"
TORLIBS="$TORLIBS -Wl,-rpath,\$ORIGIN/lib,-Bdynamic -lssl -lcrypto -levent -lz -L${OUT}/lib"

for fuzzer in src/test/fuzz/*.a; do
    output="${fuzzer%.a}"
    output="${output##*lib}"
    ${CXX} ${CXXFLAGS} -std=c++11 $LIB_FUZZING_ENGINE ${fuzzer} ${TORLIBS} -o ${OUT}/${output}

    corpus_dir="${SRC}/tor-fuzz-corpora/${output#oss-fuzz-}"
    if [ -d "${corpus_dir}" ]; then
      zip -jq ${OUT}/${output}_seed_corpus.zip ${corpus_dir}/*
    fi
done